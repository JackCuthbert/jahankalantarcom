# Documentation #

## What is this?

Repo for Jahan Kalantar's personal website. To be hosted at [http://jahankalantar.com/](http://jahankalantar.com/).

## Installation

Clone repo then install node and bower dependencies with `npm install` and `bower install`.

## Build

`grunt dev` - create an unminified development version for staging or previewing

`grunt build` - create both development version and production version for publishing

`grunt content` - compile changes made to templates to development version

`gunt preview` - run static server and watch for changes in sass files or templates

## Issues

Sitemap buiding on Windows issue, see: https://github.com/RayViljoen/grunt-sitemap/issues/9