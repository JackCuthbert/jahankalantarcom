module.exports = function(grunt) {

  // Options
  var opt = {
    src: './src',
    pages: './src/pages',
    layouts: './src/layouts',
    data: './src/data',
    production: './dist/production',
    dev: './dist/development',
    vendor: './bower_components',
    domain: 'jahankalantar.com',
    devPort: 9001,
    devHost: '0.0.0.0'
  }

  // Let's go!
  grunt.initConfig({

    pkg: grunt.file.readJSON('package.json'),
    site: grunt.file.readYAML(opt.data + '/site.yml'),

    /**
     * Clean tasks
     * 
     * all: wipe /dist folder
     * content: wipe pages from /dist/development
     *  
     */
    clean: {
      all: ['./dist'],
      content: {
        expand: true,
        cwd: './dist/development',
        src: ['/', '!/assets/**', '!robots.txt']
      }
    },

    /**
     * Copy tasks
     * 
     * dev: copy required vendor assets and imgaes to dev folder
     * production: copy development assets to production folder
     *  
     */
    copy: {
      dev: {
        files: [
          {
            // fontawesome fonts
            expand: true,
            cwd: opt.vendor + '/fontawesome/',
            src: ['fonts/**'],
            dest: opt.dev + '/assets'
          },
          {
            // fontawesome css
            expand: true,
            cwd: opt.vendor + '/fontawesome/css',
            src: ['font-awesome.min.css'],
            dest: opt.dev + '/assets/css'
          },
          {
            // bootstrap css
            expand: true,
            cwd: opt.vendor + '/bootstrap/dist/css',
            src: ['bootstrap.min.css'],
            dest: opt.dev + '/assets/css'
          },
          {
            // source images
            expand: true,
            cwd: opt.src,
            src: ['img/**'],
            dest: opt.dev + '/assets'
          }       
        ]
      },
      production: {
        files: [
          {
            expand: true,
            cwd: opt.dev + '/',
            src: ['assets/**'],
            dest: opt.production + '/'
          },
        ]
      }
    },

    /**
     * Uglify tasks
     * 
     * dist: minify and concatenate all required js files into components.js
     *  
     */
    uglify: {
      dist: {
        files: {
          // destination file
          '<%=site.development%>/assets/js/components.js': [
            // files for concat/minify
            opt.vendor + '/jquery/dist/jquery.min.js',
            opt.vendor + '/bootstrap/dist/js/bootstrap.min.js',
            opt.vendor + '/jquery.stellar/jquery.stellar.min.js',
            opt.src + '/js/custom.js'
          ]
        }
      }
    },

    /**
     * Sass tasks
     * 
     * dev: compile all sass files in src directory to development folder
     *  
     */
    sass: {
      dev: {
        options: {
          style: 'compressed'
        },
        files: [
          {
            expand: true,
            cwd: opt.src + '/sass/',
            src: ['*.scss'],
            dest: opt.dev + '/assets/css',
            ext: '.css'
          }
        ]
      }
    },

    /**
     * Assemble tasks
     * 
     * pages: assemble site content (handlebar files) in src folder to development directory
     *  
     */
    assemble: {
      options: {
        flatten: true,

        plugins: [
          'assemble-contrib-permalinks'
        ],

        helpers: [
          'handlebars-helper-moment',
        ],

        permalinks: {
          structure: ':basename/index.html'
        },

        // Templates
        layoutdir: opt.layouts,
        partials: opt.layouts + '/partials/*.hbs',
        layout: 'default.hbs',

        data: opt.data + '/*.{json,yml}',
        assets: opt.dev + '/assets',
      },

      pages: {
        files: [
          {
            src: opt.pages + '/*.{hbs,md}',
            dest: opt.dev + '/'
          },
        ]
      }

    },

    /**
     * Sitemap tasks
     * 
     * dist: create sitemap.xml in development directory
     *  
     */
    sitemap: {
      dist: {
        homepage: 'http://' + opt.domain,
        changefreq: 'monthly',
        siteRoot: 'dist/production/'
      }
    },

    /**
     * Robotstxt tasks
     * 
     * dev: create development version of robots.txt in development folder which disallows everything
     * dist: create production version of robots.txt in production folder which disallows assets/
     *  
     */
    robotstxt: {
      dev: {
        dest: opt.dev + '/',
        policy: [
            {
              ua: '*',
              disallow: '/'
            }
        ]
      },
      dist: {
        dest: opt.production + '/',
        policy: [
            {
              ua: '*',
              disallow: '/assets/'
            },
            {
              sitemap: ['http://' + opt.domain + '/sitemap.xml']
            },
            {
              crawldelay: 100
            }
        ]
      }
    },

    /**
     * Htmlmin tasks
     * 
     * dist: Minify all HTML in development folder to production folder
     *  
     */
    htmlmin: {
      dist: {
        options: {
          removeComments: true,
          collapseWhitespace: true,
          caseSensitive: true
        },
        expand: true,
        cwd: opt.dev,
        src: ['**/*.html'],
        dest: opt.production + '/'
      }
    },

    /**
     * Watch tasks
     * 
     * styles: watch sass files for changes and run appropriate tasks
     * pages: watch handlebar files in pages directory and run appropriate tasks
     *  
     */
    watch: {
      styles: {
        files: [
          opt.src + '/sass/*.scss'
        ],
        tasks: ['sass'],
        options: {
          livereload: true
        }
      },
      scripts: {
        files: [
          opt.src + '/js/*.js'
        ],
        tasks: ['uglify'],
        options: {
          livereload: true
        }
      },
      pages: {
        files: [
          opt.data + '/*.{json,yml}',
          opt.src + '/pages/*.hbs',
          opt.src + '/layouts/*.hbs',
          opt.src + '/layouts/partials/*.hbs'
        ],
        tasks: ['sass','uglify','assemble'],
        options: {
          livereload: true
        }
      }
    },

    /**
     * Connect tasks
     * 
     * dev: Create a development static server
     * production: coming soon...
     *  
     */
    connect: {
      dev: {
        options: {
          hostname: opt.devHost,
          port: opt.devPort,
          base: opt.dev + '/',
          open: {
            spawn: false,
            target: 'http://localhost:' + opt.devPort
          }
        }
      },
      dist: {
        options: {
          hostname: opt.devHost,
          port: 3000,
          base: opt.dev + '/',
          open: {
            spawn: false
          }
        }
      }
    }

  });

  // Load tasks
  grunt.loadNpmTasks('assemble');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-htmlmin');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-robots-txt');
  grunt.loadNpmTasks('grunt-sitemap');

  /**
   * Grunt CLI Tasks
   *  
   * dev: create an unminified development version for staging or previewing
   * build: create both development version and production version for publishing
   * content: compile changes made to templates to development version
   * preview: run static server and watch for changes in sass files or templates
   * serve: compile and serve on nitrous.io port then watch for changes
   *
   */
  grunt.registerTask('dev', ['clean:all','assemble','copy:dev','uglify','sass','robotstxt:dev']);
  grunt.registerTask('content',['clean:dev','assemble']);
  grunt.registerTask('build', ['dev','htmlmin','sitemap','robotstxt:dist','copy:production']);

  grunt.registerTask('preview',['connect:dev','watch']);
  grunt.registerTask('serve',['dev','connect:dist','watch']);
}