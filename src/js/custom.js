var isMobile = {
	Android: function() {
		return navigator.userAgent.match(/Android/i);
	},
	BlackBerry: function() {
		return navigator.userAgent.match(/BlackBerry/i);
	},
	iOS: function() {
		return navigator.userAgent.match(/iPhone|iPad|iPod/i);
	},
	Opera: function() {
		return navigator.userAgent.match(/Opera Mini/i);
	},
	Windows: function() {
		return navigator.userAgent.match(/IEMobile/i);
	},
	any: function() {
		return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
	}
};

jQuery(document).ready(function(){

	// enable effects only for desktop computers/laptops
	if( !isMobile.any() ){
		
		// run stellar!
		$(window).stellar();

		// Fade out avatar data when scrolling down.
		$(window).on('scroll', function() {
			var scroll = $(this).scrollTop();
			$("#masthead-holder").css({ 'opacity' : (1 - scroll/450) });

		});

	}
	$('a[href*=#]').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
    && location.hostname == this.hostname) {
      var $target = $(this.hash);
      $target = $target.length && $target
      || $('[name=' + this.hash.slice(1) +']');
      if ($target.length) {
        var targetOffset = $target.offset().top;
        $('html,body')
        .animate({scrollTop: targetOffset}, 1000);
       return false;
      }
    }
  });

});

$(window).scroll(function(){
	var $nav = $('nav');
	if ($(window).scrollTop() > 100) {
		$nav.addClass("nav-solid");
	} else {
		$nav.removeClass("nav-solid");
	}
});